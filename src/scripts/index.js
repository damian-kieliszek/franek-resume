(function() {
    'use strict';
    window.onload = function() {
        var loader = document.getElementsByClassName('loader')[0],
            loadingElements = [].slice.call(document.getElementsByClassName('loading'));
        loader.innerHTML = '';
        for (var i = 0; i < loadingElements.length; i++) {
            loadingElements[i].classList && loadingElements[i].classList.remove('loading');
        }
    }
})();
