# franek-resume

> Online Resume project of Damian Kieliszek

## Technologies

- [**Gulp**](http://gulpjs.com)
- [**Html**](https://developer.mozilla.org/es/docs/HTML/HTML5) 
- [**Sass**](http://sass-lang.com)
- [**SemanticUI**](http://semantic-ui.com/)
- [**Pug**](https://pugjs.org/api/getting-started.html)

## Install and Use

### Cloning from gitlab

```bash
git clone https://gitlab.com/damian-kieliszek/franek-resume.git
```

### Install dependencies

```bash
npm install
bower install
```

### Build development version on localhost

```bash
gulp
```

### Build production version and optimize task

```bash
gulp build
gulp optimize
```

# Author 

Damian Kieliszek - damian.kieliszek@gmail.com

# License 

The code is available under the **MIT** license. 
