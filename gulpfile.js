/* 0. import required libs */

var gulp = require('gulp'),
  sass = require('gulp-sass'),
  pug = require('gulp-pug'),
  jscs = require('gulp-jscs'),
  concat = require('gulp-concat'),
  browserSync = require('browser-sync').create(),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  minifyCss = require('gulp-cssnano'),
  uncss = require('gulp-uncss'),
  autoprefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  ftp = require('vinyl-ftp'),
  babel = require('gulp-babel'),
  cssimport = require('gulp-cssimport'),
  beautify = require('gulp-beautify'),
  sourcemaps = require('gulp-sourcemaps'),
  critical = require('critical').stream;

/* 1. baseDirs: baseDirs for the project */

var baseDirs = {
  dist: 'dist/',
  src: 'src/',
  assets: 'dist/assets/'
};

/* 2. routes: object that contains the paths */

var routes = {
  styles: {
    scss: baseDirs.src + 'styles/*.scss',
    _scss: baseDirs.src + 'styles/_includes/*.scss',
    css: baseDirs.assets + 'css/'
  },

  templates: {
    pug: baseDirs.src + 'templates/*.pug',
    _pug: baseDirs.src + 'templates/_includes/*.pug',
  },

  scripts: {
    base: baseDirs.src + 'scripts/',
    js: [
      baseDirs.src + 'scripts/*.js'
    ],
    jsmin: baseDirs.assets + 'js/'
  },

  files: {
    html: 'dist/',
    images: baseDirs.src + 'images/*',
    imgmin: baseDirs.assets + 'files/img/',
    cssFiles: baseDirs.assets + 'css/*.css',
    htmlFiles: baseDirs.dist + '*.html',
    styleCss: baseDirs.assets + 'css/style.css',
    fonts: baseDirs.src + 'libs/font-awesome/fonts/*',
    fontFiles: baseDirs.assets + 'fonts/'
  },

  deployDirs: {
    baseDir: baseDirs.dist,
    baseDirFiles: baseDirs.dist + '**',
    ftpUploadDir: 'FTP-DIRECTORY'
  }
};

/* 3. ftpCredentials: info used to deploy @ ftp server */

var ftpCredentials = {
  host: '',
  user: '',
  password: ''
};

/* 4. Compiling Tasks */

/* 4.1. Templating - compiling pug templates and browserSync streaming */

gulp.task('templates', function () {
  return gulp.src([routes.templates.pug, '!' + routes.templates._pug])
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Compiling pug.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(pug())
    .pipe(gulp.dest(routes.files.html))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'pug Compiled succesfully!',
      message: 'pug task completed.'
    }));
});

/* 4.2. SCSS - compiling scss and css files and other actions (autoprefixer, sourcemaps etc.) */

gulp.task('styles', function () {
  return gulp.src(routes.styles.scss)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Compiling SCSS.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer('last 3 versions'))
    .pipe(sourcemaps.write())
    .pipe(cssimport({}))
    .pipe(rename('style.css'))
    .pipe(gulp.dest(routes.styles.css))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'SCSS Compiled and Minified succesfully!',
      message: 'scss task completed.'
    }));
});

/* 4.3. Scripts (js) ES6 => ES5, minify and concat into a single file. */

gulp.task('scripts', function () {
  return gulp.src(routes.scripts.js)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Babel and Concat failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(concat('script.js'))
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(routes.scripts.jsmin))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'JavaScript Minified and Concatenated!',
      message: 'your js files has been minified and concatenated.'
    }));
});

/* 5. Lint the JavaScript files */

gulp.task('lint', function () {
  return gulp.src(routes.scripts.js)
    .pipe(jscs())
    .pipe(jscs.reporter());
});

/* 6. Image compressing task */

gulp.task('images', function () {
  gulp.src(routes.files.images)
    .pipe(imagemin())
    .pipe(gulp.dest(routes.files.imgmin));
});

/* 7. Fonts copying task */

gulp.task('fonts', function () {
  gulp.src(routes.files.fonts)
    .pipe(gulp.dest(routes.files.fontFiles));
});

/* 8. Deploy, deploy dist/ files to an ftp server */

gulp.task('ftp', function () {
  var connection = ftp.create({
    host: ftpCredentials.host,
    user: ftpCredentials.user,
    password: ftpCredentials.password
  });

  return gulp.src(routes.deployDirs.baseDirFiles, {
      base: routes.deployDirs.baseDir,
      buffer: false
    })
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Deploy failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(connection.dest(routes.deployDirs.ftpUploadDir))
    .pipe(notify({
      title: 'Deploy succesful!',
      message: 'Your deploy has been done!.'
    }));
});

/* 9. Preproduction beautifiying task (SCSS, JS) */

gulp.task('beautify', function () {
  gulp.src(routes.scripts.js)
    .pipe(beautify({
      indentSize: 4
    }))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Beautify failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(gulp.dest(routes.scripts.base))
    .pipe(notify({
      title: 'JS Beautified!',
      message: 'beautify task completed.'
    }));
});

/* 10. Serving (browserSync) and watching for changes in files */

gulp.task('serve', function () {
  browserSync.init({
    server: './dist/'
  });

  gulp.watch([routes.styles.scss, routes.styles._scss], ['styles']);
  gulp.watch([routes.templates.pug, routes.templates._pug], ['templates']);
  gulp.watch(routes.scripts.js, ['scripts', 'beautify']);
});

/* 11. Optimize your project */

gulp.task('uncss', function () {
  return gulp.src(routes.files.cssFiles)
    .pipe(uncss({
      html: [routes.files.htmlFiles],
      ignore: ['*:*']
    }))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: UnCSS failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(minifyCss())
    .pipe(gulp.dest(routes.styles.css))
    .pipe(notify({
      title: 'Project Optimized!',
      message: 'UnCSS completed!'
    }));
});

/* 12. Extract CSS critical-path */

gulp.task('critical', function () {
  return gulp.src(routes.files.htmlFiles)
    .pipe(critical({
      base: baseDirs.dist,
      inline: true,
      html: routes.files.htmlFiles,
      css: routes.files.styleCss,
      ignore: ['@font-face', /url\(/],
      width: 1300,
      height: 900
    }))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Critical failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(gulp.dest(baseDirs.dist))
    .pipe(notify({
      title: 'Critical Path completed!',
      message: 'css critical path done!'
    }));
});

/* 13. Define main gulp tasks */

gulp.task('dev', ['templates', 'styles', 'scripts', 'lint', 'images', 'serve']);

gulp.task('build', ['templates', 'styles', 'scripts', 'images', 'fonts']);

gulp.task('optimize', ['uncss', 'critical', 'images']);

gulp.task('deploy', ['optimize', 'ftp']);

gulp.task('default', function () {
  gulp.start('dev');
});
